import * as React from 'react';

const style = (block?: boolean) => ({
    backgroundColor: '#00D1B2',
    padding: '10px 15px',
    border: '0px',
    borderRadius: '4px',
    color: '#fff',
    width: block ? '100%' : undefined,
    marginBottom: '10px'
})

interface IIButton{
    block?: boolean
}

export default class Button extends React.Component<IIButton>{
    public render(){
        const { block } = this.props;
        return(
            <button { ...this.props }  style={style(block)}  />
        )
    }
}