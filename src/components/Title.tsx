import * as React from 'react';

const style = {
    color: '#555'
}

export default class Title extends React.Component{
    public render(){
        return(
            <h2 style={style}{ ...this.props } />
        )
    }
}