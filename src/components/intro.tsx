import * as React from 'react';

interface IIntroProps {
    text?: string  // ? =  opcional
}

interface IIntroState {
    text: string, 
    dato: number
}

export default class Intro extends React.Component<IIntroProps, IIntroState>{

    public state = {
        text: "soy un texto del estado",
        dato: 1
    }

    public render(){
        const { text } = this.props;        
        const t = text ? text : this.state.text;


        return (
            <p onClick={this.handleClick} className="App-intro">
                <span>... { t }</span>
            </p>
        )
    }


    private handleClick = () => {
        this.setState({ text: "texto actualizado" })
    }

}